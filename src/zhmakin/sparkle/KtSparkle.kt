//
//  Written by Andrey Zhmakin 2018
//

package zhmakin.sparkle

import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Point
import java.awt.event.ActionListener
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener
import java.util.*
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.Timer
import javax.swing.WindowConstants


/**
 * Main frame.
 */
class KtSparkle : JFrame("Sparkle")
{
    init
    {
        contentPane.add(Viewport())

        setLocationRelativeTo(null)

        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE

        size = Dimension(500, 500)

        isVisible = true
    }
}


const val G = 0.3

/**
 * Viewport for the sparkles.
 */
class Viewport : JPanel(), MouseMotionListener
{
    private val sparkleList = mutableListOf<Sparkle>()


    init
    {
        addMouseMotionListener(this)

        background = Color.BLACK

        val action = ActionListener()
        {
            synchronized(sparkleList)
            {
                val deadSparkleList = mutableListOf<Sparkle>()

                for (sparkle in sparkleList)
                {
                    sparkle.move()

                    if (sparkle.y > height)
                    {
                        deadSparkleList.add(sparkle)
                    }
                }

                sparkleList.removeAll(deadSparkleList)

                repaint()
            }
        }

        val timer = Timer( 50, action )

        timer.start()
    }


    private var prevPoint : Point? = null

    override fun mouseMoved(e: MouseEvent?)
    {
        if (prevPoint == null)
        {
            val sparkle = Sparkle(e!!.x.toDouble(), e.y.toDouble(), 0.0, 1.0) // <<< NullPointerException "suppressor"!, e?.x ?: 0
            sparkleList.add(sparkle)
        }
        else
        {
            val sparkle = Sparkle(e!!.x.toDouble(),
                    e.y.toDouble(),
                    (e.x - prevPoint!!.x).toDouble(),
                    (e.y - prevPoint!!.y).toDouble()) // <<< NullPointerException "suppressor"!, e?.x ?: 0
            sparkleList.add(sparkle)
        }

        prevPoint = e.point
    }


    override fun mouseDragged(e: MouseEvent?)
    {
        // Do nothing!
    }


    override fun paintComponent(g: Graphics?)
    {
        super.paintComponent(g)

        synchronized(sparkleList)
        {
            g!!.color = Color.YELLOW

            for (sparkle in sparkleList)
            {
                sparkle.paint(g)
            }
        }
    }
}


val random = Random(0)


/**
 * Represents a sparkle.
 */
class Sparkle(var x_: Double, var y_: Double, var ax: Double, var ay: Double)
{
    val x: Int
        get() = Math.round(x_).toInt()

    val y: Int
        get() = Math.round(y_).toInt()

    fun move()
    {
        x_ += ax
        y_ += ay

        ay += G
    }

    fun paint(g: Graphics)
    {
        for (i in 1..7)
        {
            val oX = random.nextInt(20) - 10
            val oY = random.nextInt(20) - 10

            g.drawLine(x, y, x + oX, y + oY)
        }
    }
}


fun main(args: Array<String>)
{
    KtSparkle()
}
